#!/usr/bin/env groovy

def call() {
    echo "testing on $BRANCH_NAME..."
    dir("app") {
        sh "npm install"
        sh "npm run test"
    }
}